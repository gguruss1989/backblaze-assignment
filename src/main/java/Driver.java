import com.backblaze.b2.client.B2StorageClient;
import com.backblaze.b2.client.B2StorageClientFactory;
import com.backblaze.b2.client.exceptions.B2Exception;
import com.backblaze.b2.client.structures.B2Bucket;
import com.backblaze.b2.client.structures.B2FileVersion;
import config.ConfigUtil;
import config.Configuration;
import model.Result;
import strategy.ContentRequester;
import strategy.TempDownloadStrategy;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;


public class Driver {

    private static final String KEYWORD_TO_FIND = "a";

    private static final String DEFAULT_USER_AGENT = "backblaze-assignment";

    public static void main(String[] args) throws B2Exception, IOException {
        if (args.length != 2) {
            printMessage("Please Insert: APP_ID APPKEY");
        }

        new Driver().run(args);
    }

    private static void printMessage(String message) {
        System.err.println(message);
        System.exit(1);
    }

    private void run(final String[] args) throws B2Exception, IOException {

        ConfigUtil.configure(args);
        Configuration.getInstance().setUserAgent(DEFAULT_USER_AGENT);
        Configuration.getInstance().setKeywordToFind(KEYWORD_TO_FIND);

        printResult();
    }

    private void printResult() throws B2Exception, IOException {

        ExecutorService pool = null;
        try (
                B2StorageClient client = B2StorageClientFactory
                        .createDefaultFactory()
                        .create(Configuration.getInstance().getAppId(),
                                Configuration.getInstance().getAppKey(),
                                Configuration.getInstance().getUserAgent())) {

            pool = Executors.newFixedThreadPool(Configuration.getInstance().getNumberOfThreads());

            if (client.buckets().isEmpty()) {
                printMessage("No buckets found");
            }

            List<CompletableFuture<Result>> futureResults = new ArrayList<>();
            for (final B2Bucket bucket : client.buckets()) {
                for (final B2FileVersion file : client.fileNames(bucket.getBucketId())) {

                    //all files in the bucket will be downloaded to this directory
                    Path tempDir = Files.createTempDirectory(bucket.getBucketName());
                    ContentRequester contentRequester = new TempDownloadStrategy(bucket.getBucketName(), file.getFileName(), tempDir);
                    //wrapping them up in completable futures to run parallel and get results
                    CompletableFuture<Result> resultContentPerFile = CompletableFuture
                            .supplyAsync(() -> contentRequester.requestContent(),
                                    pool);
                    futureResults.add(resultContentPerFile);
                }
            }

            //after all completable futures are complete
            CompletableFuture<Void> allOfFuture = CompletableFuture
                    .allOf(futureResults.toArray(new CompletableFuture[futureResults.size()]));

            //wrap up the value to List<Result>
            CompletableFuture<List<Result>> results = allOfFuture
                    .thenApply(v ->
                    {
                        return futureResults.stream()
                                .map(CompletableFuture::join)
                                .collect(Collectors.toList());
                    });

            //we then iterate through results and sort it based on count and filename
            results.thenAccept(r ->
                    r.stream()
                            .sorted(Comparator.comparing(Result::getCount)
                                    .thenComparing(Result::getFileName)).
                            forEach(t -> System.out.println(t.getCount() + " " + t.getFileName())));


        } finally {
            if (pool != null)
                pool.shutdown();
        }
    }
}
