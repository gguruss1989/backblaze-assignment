package model;

/**
 * Holds Result Model
 */
public class Result {

    private final String fileName;

    private final long count;

    public Result(String fileName, long count) {
        this.fileName = fileName;
        this.count = count;
    }

    public String getFileName() {
        return fileName;
    }

    public long getCount() {
        return count;
    }

    @Override
    public String toString() {
        return count + " " + fileName;
    }
}
