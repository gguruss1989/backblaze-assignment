package strategy;

import com.backblaze.b2.client.B2StorageClient;
import com.backblaze.b2.client.B2StorageClientFactory;
import com.backblaze.b2.client.contentHandlers.B2ContentFileWriter;
import com.backblaze.b2.client.exceptions.B2Exception;
import com.backblaze.b2.client.structures.B2DownloadByNameRequest;
import config.Configuration;
import model.Result;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

/**
 * Strategy of downloading files and iterating through the file finding the keyword
 */
public class TempDownloadStrategy implements ContentRequester {

    private final String bucketName;

    private final String filename;

    private final Path tempDir;

    public TempDownloadStrategy(String bucketName, String filename, Path tempDir) {
        this.bucketName = bucketName;
        this.filename = filename;
        this.tempDir = tempDir;
    }

    @Override
    public Result requestContent() {
        try (B2StorageClient client = B2StorageClientFactory
                .createDefaultFactory()
                .create(Configuration.getInstance().getAppId(),
                        Configuration.getInstance().getAppKey(),
                        Configuration.getInstance().getUserAgent())) {

            final B2DownloadByNameRequest request = B2DownloadByNameRequest
                    .builder(bucketName, filename)
                    .build();

            File fileToCreate = new File(tempDir + File.separator +
                    bucketName + "_" + filename);

            final B2ContentFileWriter handler = B2ContentFileWriter
                    .builder(fileToCreate)
                    .setVerifySha1ByRereadingFromDestination(true)
                    .build();

            client.downloadByName(request, handler);
            if (Files.exists(fileToCreate.toPath())) {
                long count = readFileAndCountNeedle(fileToCreate, Configuration.getInstance().getKeywordToFind());
                return new Result(filename, count);
            }
        } catch (B2Exception | IOException e) {
            System.out.println("Error :" + e);
            e.printStackTrace();
        }
        return null;
    }

    private long readFileAndCountNeedle(File file, String keywordToFind) throws IOException {
        long count = 0;
        try (BufferedReader reader = Files.newBufferedReader(Paths.get(file.getAbsolutePath()))) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                if (line.toLowerCase().contains(keywordToFind))
                    count = count + Arrays.stream(line.toLowerCase().split("")).filter(keywordToFind::equals).count();
            }
        }
        return count;
    }
}
