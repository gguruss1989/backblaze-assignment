package strategy;


import model.Result;

public interface ContentRequester {

    /**
     *
     * @return Retrieves content, parses the content and returns result
     *
     */
    Result requestContent();
}
