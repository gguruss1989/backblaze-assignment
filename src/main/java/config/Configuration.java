package config;

/**
 * configuration model
 */
public class Configuration {


    private static Configuration instance;

    //number of threads for executor service
    private Integer numberOfThreads;

    private String keywordToFind;

    private String appId;

    private String appKey;

    private String userAgent;

    public static Configuration getInstance() {
        return instance;
    }

    public static void setInstance(Configuration instance) {
        Configuration.instance = instance;
    }

    public Integer getNumberOfThreads() {
        return numberOfThreads;
    }

    public void setNumberOfThreads(Integer numberOfThreads) {
        this.numberOfThreads = numberOfThreads;
    }

    public String getKeywordToFind() {
        return keywordToFind;
    }

    public void setKeywordToFind(String keywordToFind) {
        this.keywordToFind = keywordToFind;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }
}
