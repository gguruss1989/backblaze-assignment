package config;

/**
 * Utility class to setup configuration data
 */
public final class ConfigUtil {

    private static final int DEFAULT_THREAD_COUNT = 20;

    private static final String DEFAULT_KEYWORD = "a";

    private ConfigUtil() {

    }

    public static void configure(final String[] args) {

        final Configuration config = new Configuration();
        config.setNumberOfThreads(DEFAULT_THREAD_COUNT);
        config.setKeywordToFind(DEFAULT_KEYWORD);
        config.setAppId(args[0]);
        config.setAppKey(args[1]);

        Configuration.setInstance(config);
    }

}
